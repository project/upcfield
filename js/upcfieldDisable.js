
/**
 * @file
 * Disable upcfield barcode field if the auto_value checkbox has been
 * clicked.
 */

/**
 * Define the upcfieldDisable behavior for upcfield_widget form elements.
 */
Drupal.behaviors.upcfieldDisable = function(context) {
  $('.upcfield-widget:not(.upcfielddisable-processed)')
    .addClass('upcfielddisable-processed')
    .each(function() {
      var inputDescendants = $('input', this);
      var autoValueID = inputDescendants.eq(0).attr('id');
      var valueID     = inputDescendants.eq(1).attr('id');
    
      // Toggle the value element on and off based on
      // auto_value's checked attribute. We use .change()
      // here, becuase jQuery's .toggle() has a bug
      // that doesn't set the checked attribute
      // of checkboxes correctly.
      $('#' + autoValueID).change(function() {
        if ($(this).attr('checked') === true) {
          $('#' + valueID).attr('disabled', 'disabled');
        }
        else {
          $('#' + valueID).removeAttr('disabled');
        }
      });
    });
};