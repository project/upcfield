
About
-----

The UPC Barcode Field module defines a UPC barcode CCK field in Drupal. The
module is capable of turning a 12 digit UPC string into a PNG barcode image
that can be printed out and scanned by a capable barcode reader.

This module has been developed for a specific use-case for the uCount module,
but it has been designed with the goal of being independent of that system.

How to use
----------

Follow the instructions in INSTALL.txt to install the module. Then, navigate
to the default Content types configuration screen located at this Drupal
path: admin/content/types/list

Note: You must have the 'administer content types' permission to access
this page.

Add the UPC Barcode field to a node type. There is currently only one widget
type available, which is called UPC Barcode widget. On the widget configuration
page there are a few setting options specific to the UPC Barcode field:

Barcode Image Height - This lets you configure the height of the barcode image
                       that will be generated when a user creates this content.

Barcode Image Path   - This is a path relative to your Drupal files directory
                       that barcode images will be stored.

Barcode Prefix       - Each field can have its own unique barcode prefix
                       which can be set here. This value will automatically
                       increment each time a new field is created. Note: This
                       prefix is only maintained when using the UPC Barcode
                       widget's auto-value behavior, see below.

Disable images       - This checkbox allows you to disable image creation
                       when content using this field is created. This can be
                       useful when importing a large number of content as
                       image generation is an intensive process.

UPC Barcode widget auto-value checkbox
--------------------------------------

There is a checkbox on all UPC Barcode widgets that allows the user to let the
UPC Barcode Field module determine the value of the barcode. This checkbox's
initial value can be set in the Default section on the widget configuration
form.

When this checkbox is set, the UPC Barcode Field module will use an internal
counter to determine the next available barcode value. It will also prepend the
field's barcode Prefix value (see above).

Contact and Support
-------------------

All issues with this module should be filed on the project page at:

http://drupal.org/project/upcfield